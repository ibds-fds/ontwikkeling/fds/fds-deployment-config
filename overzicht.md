# Overzicht FDS 

TODO: begrippen gelijktrekken 

# Data Aanbieders

Aanwezig
- Haal-Centraal-BRP: https://digilab.overheid.nl/docs/testvoorzieningen/static-brp-test-data/

Nog te deployen
- BAG API: https://github.com/lvbag/BAG-API
- KvK: https://developers.kvk.nl/documentation


```plantuml
!include  <C4/C4_Context.puml>
!include <office/Users/user.puml>

LAYOUT_WITH_LEGEND()


title FDS MVP huidige setup
    Person(burger, "Burger")
cloud FSC {
    Boundary(fsc_aanbieders, "Data aanbieders") {
        System(kentekens, "Basisregistratie Fictieve Kentekens")
    }
}
cloud Overig{
    Boundary(other_aanbieders, "Data aanbieders") {
    }
}
Rel(burger, FSC, "")
Rel(burger, Overig, "")
```


```plantuml
@startuml
!include https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml
    title FDS MVP
    Person(burger, "Burger")
    System_Boundary(fds, "FDS") {
        Boundary(aanbieders, "Data aanbieders") {
            System(aanbieder, "Aanbieder")
            System_Ext(aanbieder0, "Haal-Centraal-BRP", "")
            System_Ext(aanbieder1, "BAG API", "(niet aanwezig)")
            System_Ext(aanbieder2, "BAG API", "(niet aanwezig)")
        }
        Boundary(afnemers, "Data afnemers") {
            System(afnemer, "Afnemer")
            System_Ext(afnemer0, "Onbekend", "(Niet aanwezig)")
            System_Ext(afnemer1, "Onbekend", "(Niet aanwezig)")
        }
        System(demo, "Demo GUI", "(Niet aanwezig)")
        System(portaal, "Portaal", "(Niet aanwezig)")
        System(fsc, "FSC", "(niet aanwezig)")
        System(ledenlijst, "Ledenlijst", "(Niet aanwezig)")
        System(metadata, "DCat Metadata", "(Niet aanwezig)")
    }
    Rel(burger, portaal, "gebruikt")
    Rel(burger, demo, "gebruikt")
    Rel(portaal, aanbieder, "")
    Rel(portaal, fsc, "")
    Rel(afnemer, aanbieder, "maakt gebruik van")
@enduml
```